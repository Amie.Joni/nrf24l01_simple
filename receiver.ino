// -----------------------------------------------------------------------------
// this is the receiver
// this RF communication use nrf24l01 board
// -----------------------------------------------------------------------------

#include <SPI.h>
#include <RF24.h>
#include <L298N.h>

struct Data{
  int XaxisValue = 0;                  //  gyro x values for forward and backward
  int YaxisValue = 0;                  //  gyro y values for left and right
  int ZaxisValue = 0;                  //  gyro z values not used
};

RF24 radio(7,8);
byte addresses[][6] = {"1Node","2Node"};

//Pins
const int buzzer = A5;
const int ENA = 10;
const int IN1 = 9;
const int IN2 = 6;
const int IN3 = 5;
const int IN4 = 4;
const int ENB = 3;

L298N driver(ENA,IN1,IN2,IN3,IN4,ENB);

int speed = 200;

void setup() {
  radio.begin();             // Initialize radio
  radio.setPALevel(RF24_PA_LOW);    // Set the power output to low
  radio.openWritingPipe(addresses[0]);
  radio.openReadingPipe(1,addresses[1]);
  radio.startListening();

  //  Buzzer
  pinMode(buzzer,OUTPUT);

  Serial.begin(9600);  //  Serial monitor start
}


void loop() {
  struct Data inputInfo;

    if(radio.available()){
        radio.read(&inputInfo,sizeof(inputInfo));
        delay(50);// Wait until msg is received
        treatMove(inputInfo.XaxisValue);
        treatTurn(inputInfo.YaxisValue);
    } else {
          drive(L298N::MOTOR_B,0,LOW,LOW);
          drive(L298N::MOTOR_A,0,LOW,LOW);
          Serial.println("No radio");
          digitalWrite(buzzer,HIGH);
          delay(500);
          digitalWrite(buzzer,LOW);
          delay(500);
    }

  //  Sending readed data to serial monitor
  Serial.print(inputInfo.XaxisValue);
  Serial.print("\t");
  Serial.print(inputInfo.YaxisValue);
  Serial.print("\t");
  Serial.println(inputInfo.ZaxisValue);

}

//  Treat move values and decide which direction
void treatMove(int m){
  if(m > 1000){goBackward();Serial.println("Backward");}
  else if(m < -1000){goForward();Serial.println("Forward");}
  else{
    Serial.println("STOP MOVE");
    drive(L298N::MOTOR_B,0,LOW,LOW);
    }
}

//  Treat turn values and decide which side
void treatTurn(int t){
  if( t < -1000 ){goLeft();Serial.println("Left");}
  else if( t > 1000 ){goRight();Serial.println("Right");}
  else{
    Serial.println("STOP TURN");
    drive(L298N::MOTOR_A,0,LOW,LOW);
    }
}


void goForward(){
  drive(L298N::MOTOR_B,speed,LOW,HIGH);
}

void goBackward(){
  drive(L298N::MOTOR_B,speed,HIGH,LOW);
}

void goLeft(){
  drive(L298N::MOTOR_A,200,LOW,HIGH);
}

void goRight(){
  drive(L298N::MOTOR_A,200,HIGH,LOW);
}

void drive(int motor,int s,int state1,int state2) {
  driver.setup_motor(motor,state1,state2);
  driver.drive_motor(motor,s);
}
