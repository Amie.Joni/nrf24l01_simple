// -----------------------------------------------------------------------------
// this is the transmitter
// this RF communication use nrf24l01 board
// -----------------------------------------------------------------------------

#include <SPI.h>
#include <RF24.h>
#include<Wire.h>

struct Data{
  int XaxisValue = 0;                  //  gyro x values for forward and backward
  int YaxisValue = 0;                  //  gyro y values for left and right
  int ZaxisValue = 0;                  //  gyro z values not used
};

const int MPU_addr=0x68;  // I2C address of the MPU-6050

RF24 radio(9,10);
byte addresses[][6] = {"1Node","2Node"};


void setup() {
  radio.begin();                        // Get the transmitter ready
  radio.setPALevel(RF24_PA_LOW);        // Set the power to low
  radio.openWritingPipe(addresses[1]);  // Where we send data out
  radio.openReadingPipe(1,addresses[0]);// Where we receive data back

  Wire.begin();
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B);  // PWR_MGMT_1 register
  Wire.write(0);     // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);

  Serial.begin(9600);  //  serial monitor start
}


 void loop() {
  struct Data inputInfo;

  Wire.beginTransmission(MPU_addr);
  Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr,14,true);  // request a total of 14 registers
  inputInfo.XaxisValue=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  inputInfo.YaxisValue=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  inputInfo.ZaxisValue=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)

  //  sending readed data to serial monitor
  Serial.print(inputInfo.XaxisValue);
  Serial.print("\t");
  Serial.print(inputInfo.YaxisValue);
  Serial.print("\t");
  Serial.println(inputInfo.ZaxisValue);

  //Stop listening and begin transmitting
  radio.stopListening();                                 // Stop listening and begin transmitting
  //delay(500);

  if(radio.write(&inputInfo, sizeof(inputInfo)),Serial.println("\n sent inputInfo \n "));         //Send inputInfo data

  radio.startListening();
 }
